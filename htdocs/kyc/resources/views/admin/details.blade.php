@extends('admin.layout.dashboard')

@section('content')
<!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-two">
                            <header>
                                <div class="avatar">
                                    <img src="https://bitexchange.cash/kyc/storage/app/{{ $user->avatar }}" alt="{{ $user->first_name }}" />
                                </div>
                            </header>

                            <h3>{{ $user->first_name }} {{ $user->last_name }}</h3>
                            <div class="desc">
                               {{ $user->email }}
                            </div>
                            <div class="contacts">
                                <a href="{{ $user->twitter_profile }}"><i class="fa fa-twitter"></i></a>
                                <a href="{{ $user->linkedin_profile }}"><i class="fa fa-linkedin"></i></a>
                                <a href="mailto:{{ $user->email }}"><i class="fa fa-envelope"></i></a>
                                <div class="clear"></div>
                            </div>
                            <div style="text-align: center;">
                                @if($user->approval == 'PENDING')
                                <a href="{{ route('admin.approval', [ 'id' => $user->id, 'approval' => 'APPROVED' ]) }}" class="btn btn-success">Approve</a>
                                <a href="{{ route('admin.approval', [ 'id' => $user->id, 'approval' => 'DECLINED' ]) }}" class="btn btn-danger">Decline</a>
                                @elseif($user->approval == 'APPROVED')
                                <a href="{{ route('admin.approval', [ 'id' => $user->id, 'approval' => 'DECLINED' ]) }}" class="btn btn-danger">Decline</a>
                                @else
                                <a href="{{ route('admin.approval', [ 'id' => $user->id, 'approval' => 'APPROVED' ]) }}" class="btn btn-success">Approve</a>
                                @endif
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile_info" role="tab">Profile Information</a> </li>
                        <!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#security" role="tab">Security</a> </li> -->
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#kyc" role="tab">KYC / AML Verification</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#contact" role="tab">Contact Preference</a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="profile_info" role="tabpanel">
                            <div class="card-body">

                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>First Name</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->first_name }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Last Name</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->last_name }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Phone</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->phone }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->email }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>Country</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->country }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>City</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->city }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>Address</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->address }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>Postal Code</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->postal_code }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>Joined at</strong>
                                        <br>
                                        <p class="text-muted">{{ date('d M Y', strtotime($user->created_at)) }}</p>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                     <!-- <strong>Referral Link</strong> -->
                                        <br>
                                        <!-- <p class="text-muted"><a href="{{ route('ref') }}?refid={{ $user->referral_code }}">{{ route('ref') }}?refid={{ $user->referral_code }}</a></p> -->
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>Twitter Profile</strong>
                                        <br>
                                        <p class="text-muted"><a href="{{ $user->twitter_profile }}">{{ $user->twitter_profile }}</a></p>
                                    </div>
                                    <div class="col-md-6 col-xs-6"> <strong>Linkedin Profile</strong>
                                        <br>
                                        <p class="text-muted"><a href="{{ $user->linkedin_profile }}">{{ $user->linkedin_profile }}</a></p>
                                    </div>

                                </div>
                                <hr>

                            </div>
                        </div>
                        <!--second tab-->
                        <div class="tab-pane" id="security" role="tabpanel">
                            <div class="card-body">
                                <br> <br>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Enabled 2 Factor Authentication</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->preference->E2F? 'YES' : 'NO' }}</p>
                                    </div>

                                    <div class="col-md-6 col-xs-6 b-r"> <strong>Enabled SMS Authentication</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->preference->ESMS? 'YES' : 'NO' }}</p>
                                    </div>

                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="tab-pane" id="kyc" role="tabpanel">
                            <div class="card-body">
                                <br> <br>
                                <div class="row">
                                    <div class="col-md-12 col-xs-6 b-r"> <strong>ID Proof Type</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->preference->id_proof_type }}</p>
                                    </div>

                                    <div class="col-md-12 col-xs-6 b-r"> <strong>ID Proof</strong>
                                        <br>
                                        <a href="{{ $user->preference->id_proof }}" data-lightbox="id_proof" data-title="ID Proof">
                                        <img src="https://bitexchange.cash/kyc/storage/app/{{ $user->preference->id_proof }}" class="thumbnail" height="200">
                                        </a>
                                    </div>

                                    <div class="col-md-12 col-xs-6 b-r"> <strong>Address Proof Type</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->preference->address_proof_type }}</p>
                                    </div>

                                    <div class="col-md-12 col-xs-6 b-r"> <strong>Address Proof</strong>
                                        <br>
                                        <a href="{{ $user->preference->address_proof }}" data-lightbox="address_proof" data-title="Address Proof">
                                        <img src="https://bitexchange.cash/kyc/storage/app/{{ $user->preference->address_proof }}" class="thumbnail" height="200">
                                        </a>
                                    </div>

                                    <div class="col-md-12 col-xs-6 b-r"> <strong>ID Card</strong>
                                        <br>
                                        <a href="{{ $user->preference->id_card }}" data-lightbox="id_card" data-title="ID Card">
                                        <img src="https://bitexchange.cash/kyc/storage/app/{{ $user->preference->id_card }}" class="thumbnail" height="200">
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>

                         <div class="tab-pane" id="contact" role="tabpanel">
                            <div class="card-body">
                                <br> <br>
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 b-r"> <strong>SMS</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->preference->contact_email? 'YES' : 'NO' }}</p>
                                    </div>

                                    <div class="col-md-6 col-xs-6 b-r"> <strong>EMAIL</strong>
                                        <br>
                                        <p class="text-muted">{{ $user->preference->contact_email? 'YES' : 'NO' }}</p>
                                    </div>

                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>

        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

@endsection
