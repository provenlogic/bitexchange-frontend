@extends('layouts.auth')

@section('content')

@include('layouts.nav')
<div class="page-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <br>
                <br>
                <br>
                <h4 class="text-center"><strong>Contact Preference</strong></h4>
                <br>
                <br>
                <br>
            </div>
            @include('layouts.progress')
            <br>
            <br>
            <br>
            @include('layouts.tab')
        </div>

        <form method="POST" action="{{ route('contact.update') }}" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif

         @if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
        @endif

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">

                    <div class="pro card">
                        <div class="card-header">
                            <h6><strong>Email</strong></h6>
                        </div>
                        <div class="card-body">
                            <h4>Can we contact you via. email?</h4>
                            <p>On/Off
                                <label class="switch">
                                    <input type="checkbox" name="contact_email" @if(auth()->user()->preference->contact_email) checked="" @endif>
                                    <span class="slider round"></span>
                                </label>
                            </p>
                            <div style="float: right;"><button class="btn btn-yellow">Submit</button></div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">

                    <div class="pro card">
                        <div class="card-header">
                            <h6><strong>SMS</strong></h6>
                        </div>
                        <div class="card-body">
                            <h4>Can we contact you via. SMS?</h4>
                            <p>On/Off
                                <label class="switch">
                                    <input type="checkbox" name="contact_sms" @if(auth()->user()->preference->contact_sms) checked="" @endif>
                                    <span class="slider round"></span>
                                </label>
                            </p>
                            <div style="float: right;"><button class="btn btn-yellow">Submit</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        </div>
</div>
@include('layouts.footer')
@endsection
